" file name is minimal.vimrc

" set path
set runtimepath+=~/.local/share/nvim/site/pack/packer/start/vim-quickrun/

 " turn on plugins
set nocompatible
filetype plugin indent on
syntax enable

" let s:foo = {
"     \  'command'                         : 'fsharpi --readline-'
"     \ ,'runner'                          : 'concurrent_process'
"     \ ,'runner/concurrent_process/load'  : '#load "%S";;'
"     \ ,'runner/concurrent_process/prompt': '> '
" \}
"
" autocmd BufReadPost  *.fsx  call quickrun#run( s:foo )
