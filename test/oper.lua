-- Automatically switch between dark and light themes during day and night
-- Randomly switch themes during the day
-- @see core.util.tokyonight.lua
local sta = function()
  -- @bug: https://stackoverflow.com/questions/20154991/generating-uniform-random-numbers-in-lua
  math.randomseed(os.time())
  local theme = "storm"
  local hour_number = tonumber(os.date("%H"))
  local status = math.random(0, 1)
  -- default theme is storm in day
  -- toggle night theme in nighttime
  if hour_number < 8 or hour_number > 20 or status == 1 then
    theme = "night"
    print("test")
  end
end

sta()
