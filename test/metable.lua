--[[ Lua 查找一个表元素时的规则，其实就是如下 3 个步骤:

1.在表中查找，如果找到，返回该元素，找不到则继续
2.判断该表是否有元表，如果没有元表，返回 nil，有元表则继续。
3.判断元表有没有 __index 方法，如果 __index 方法为 nil，则返回 nil；如果 __index 方法是一个表，则重复 1、2、3；如果 __index 方法是一个函数，则返回该函数的返回值。
该部分内容来自作者寰子：https://blog.csdn.net/xocoder/article/details/9028347 ]]

local t1 = { 1 }

local t2 = { 1, 33, 44 }

setmetatable(t1, { __index = t2 })

print(t1[3])
