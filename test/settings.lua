local M = {}

M.Lua = {
  diagnostics = {
    globals = { "vim" },
  },
}

return M
