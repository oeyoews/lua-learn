-- bug: :so % is using nvim lua, else use system lua
-- @see https://stackoverflow.com/questions/31432034/missing-functions-in-table-library-in-lua
-- @see https://blog.csdn.net/fwb330198372/article/details/88579361
-- @see https://toutiao.io/posts/ldizz9/preview
local tbl = {
  9,
  key = "value",
  { 1, 2, 3 },
  { 1, 2, 3 },
}

-- print(type(table.maxn(tbl)))

print(#tbl) -- just get length of array table, hashmap not

print(next(tbl, nil))

for i, v in pairs(table) do
  print(i, v)
end

print(_VERSION)

for index, value in pairs(math) do
  print(index, value)
end

print(math.pow(2, 4))
