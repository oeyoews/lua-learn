local utest = {
  "22",
  "88",
  nil,
  "000",
  "hello",
  demo = {
    "heloo",
  },
}

local tbl = {
  demo = {
    demo = {
      1,
    },
  },
}

-- NOTE: use table.unpack, not unpack(in version >= 5.1)
print(table.unpack(utest))
print(#utest)
-- print(table.unpack(tbl))
