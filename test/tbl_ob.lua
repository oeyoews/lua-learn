cal = {
  a = 0,
  b = 0,
  result = 0,
  ---  add method
  ---@return number
  add = function()
    return cal.a + cal.b
  end,
  --- show
  show = function()
    print(cal.result)
  end,
}

cal.a = 99
cal.b = 88
print(cal.a)
cal.add()
cal.show()
