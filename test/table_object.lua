local tbl = {
  a = 1,
  b = 2,
  getter = function(method)
    print(method)
  end,
}

--- add method like class
---@param a number
---@param b number
---@return number
tbl.add = function(a, b)
  return a + b
end

tbl.age = 99

add_res = tbl.add(1, 2)

print(add_res)

print(tbl.age)

tbl.getter(tbl.add(1, 3))
