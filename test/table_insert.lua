local test = {
  1,
  2,
  3,
}

local inser_table = {
  99,
  88,
  44,
}

table.insert(test, inser_table)

table.insert(test, 33)
test[#test + 1] = 399
test[#test + 1] = 39999

for _, v in ipairs(test) do
  print(v)
end
