-- 数字下标连续
-- tabTemp1
local tabTemp1 = "Hello"

local tedmo = "world"

require("pcall_modules").setup("test", tabTemp1)

print(table.concat(tabTemp1, "."))
--[[
-- tabTemp2
--
local tabTemp2 =
{
  [1] = "c",
  [2] = "c++",
  [3] = "lua",
  [4] = "kotlin",
  [5] = "python",
  [6] = "go",
  [7] = "sql",
  [8] = "php"
};

print("length2: " .. (#tabTemp2))
print(table.concat(tabTemp2, ";"))

-- tabTemp3
local tabTemp3 =
{
  "c",
  "c++",
  "lua",
  a = 10,
  b = 20,
  "kotlin",
  "python",
  "go",
  "sql",
  "php"
};

print("length3: " .. (#tabTemp3))
print(table.concat(tabTemp3, ";"))

-- tabTemp4
local tabTemp4 =
{
  "c",
  "c++",
  "lua",
  a = 10,
  b = 20,
  "kotlin",
  "python",
  "go",
  "sql",
  "php",
  [9] = "java",
  [10] = "swift"
};

print("length4: " .. (#tabTemp4))
print(table.concat(tabTemp4, "."))

--[[
length1: 8
c;c++;lua;kotlin;python;go;sql;php
length2: 8
c;c++;lua;kotlin;python;go;sql;php
length3: 8
c;c++;lua;kotlin;python;go;sql;php
length4: 10
c;c++;lua;kotlin;python;go;sql;php;java;swift
--]]
--]]
