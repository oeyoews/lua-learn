local test = function()
  local i = 0
  local n = 99
  local function dtest()
    i = i + 1
    print(i + n)
  end

  return dtest
end

local g = test()
-- run dtest multi times ,, not test( ) function
g()
g()
g()
g()
