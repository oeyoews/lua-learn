--[[ __newindex 元方法用来对表更新，__index则用来对表访问 。

当你给表的一个缺少的索引赋值，解释器就会查找__newindex 元方法：如果存在则调用这个函数而不进行赋值操作。

以下实例演示了 __newindex 元方法的应用： ]]
local t1 = {}

local t2 = {}

setmetatable(t1, { __newindex = t2 })

t1.v1 = 99

print(t2.v1)
