local original = {
  demo = 1,
  demo2 = 2,
  spe = function(key)
    if key == "demo3" then
      return 2200
    end
  end,
}

local mt = {
  test = 11,
}

local uptbl = setmetatable(mt, {
  __index = original,
})

print(uptbl.spe("demo3"))
