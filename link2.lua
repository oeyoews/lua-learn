local node = {}

--- new
---@param val number
---@return string
node.new = function(val)
  return {
    data = val,
    next = nil,
  }
end

--- insert
---@param n number
---@param val number
node.insert = function(n, val)
  n.next = node.new(val)
end
