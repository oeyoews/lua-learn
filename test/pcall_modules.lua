-- @module: pcall.lua
-- @ref: core.utils.init.lua && user.modules.lua

--- just suit for three layer modules
---@param entry string
setup = function(entry)
  -- setup second entry default value
  entry = entry or "modules"
  -- second entry
  local modules = {
    "lang",
    "options",
    "ui",
  }

  local ddd = {
    lang = {
      1,
      2,
      44,
      55,
    },
    options = {
      "demo",
      "tele",
      "hello",
    },
    ui = {
      -- 1, 3, 55
    },
  }

  -- storage error modules in for loop
  local error_modules = {}
  -- PERF: logging?
  local error_logs = {}

  for _, module in ipairs(modules) do
    for _, load_module in ipairs(ddd[module]) do
      print(load_module)
      -- print(load_module)
      local path = { entry, module, load_module }
      -- print(module[load_module])
      -- link path to load module
      connect_path = table.concat(path, ".")
      -- link path use separator for logging
      connect_path2 = table.concat(path, "  ")
      -- local status_ok, _ = pcall(require, connect_path)
      -- local status_ok, error_log = pcall(require, connect_path)
      print(connect_path)
      -- if not status_ok then
      --   -- storage error_module in tabale
      --   error_modules[#error_modules + 1] = connect_path2
      --   -- storage logging message
      --   error_logs[#error_logs + 1] = error_log
      -- end
    end
  end

  -- link error_modules path from for loop
  local error_msg = table.concat(error_modules, "\n")

  local error_tree = table.concat(error_logs, "\n")

  -- if #error_modules ~= 0 then
  --   async.run(function()
  --     notify("Failed to loaded modules \n" .. error_msg, "info", { title = "Modules" }).events.close()
  --     if debug_mode then
  --       notify("Error Messages \n" .. error_tree, "error", { title = "Debug" })
  --     end
  --   end)
  -- end
end

setup()
