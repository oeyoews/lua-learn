-- 根节点
list = nil
-- 在链表头部插入一个值为v的节点
list = {
  next = list,
  value = "Hello",
}
-- 遍历链表
local l = list
while l do
  print(l.value)
  l = l.next
end

print(type(list.next))
print(type(list))
