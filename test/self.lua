local t = {
  a = 1,
  b = 99,
}

function t:add(a, b)
  a = a or self.a
  b = b or self.b
  return a + b
end

print(t:add(999999, 88))
