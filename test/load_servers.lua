local ok, servers2 = pcall(require, "servers")

if not ok then
  vim.notify("servers not founded")
end

for _, v in pairs(servers2) do
  print(v)
end
