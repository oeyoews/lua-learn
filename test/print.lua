local test = {}

test.boy = {
  1,
  2,
  3,
}

local demo = "version"

print("Hello, World")

print(string.len(demo))

print(string.rep(demo, 5))

print(string.upper(demo))

print(string.byte(demo))

print(test.boy)
-- print(table.maxn(test.boy))

-- print(table(#test))
print(#demo)
print(#test.boy)

-- ref: Lua lists have a base index of 1 because it was thought to be most friendly for non-programmers, as it makes indices correspond to ordinal element positions.
