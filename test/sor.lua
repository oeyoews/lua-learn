local modules = {
  lang = {
    "lang",
  },
  options = {
    "options",
  },
  a = {
    "ademo",
  },
}

entry = {
  "lang",
  "options",
  "utils",
}

table.sort(modules)

for k, _ in pairs(modules) do
  print(k)
end
