--[[ local TITLE = "nvim-lsp-installer"

local has_notify_plugin = pcall(require, "notify")
level = level or vim.log.levels.INFO
if has_notify_plugin then
  vim.notify("msg", "warn", {
    title = TITLE,
  })
else
  vim.notify(("[%s] %s"):format(TITLE, "warn"), "warn")
end
--]]

local async = require("plenary.async")
local notify = require("notify").async

async.run(function()
  notify("Let's wait for this to close").events.close()
  notify("It closed!")
end)
