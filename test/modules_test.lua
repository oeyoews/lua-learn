-- local package = "modulels.plugins"

local modules = {
  packer = {
    1,
    2,
    3,
  }, -- first plugins list
  test333 = {
    4,
    6,
    9,
  },
  atest33 = {
    4,
    6,
    9,
  },
}

table.sort(modules)

for entry, load_module in pairs(modules) do
  print(entry)
  for _, v in ipairs(load_module) do
    print(v)
  end
end
