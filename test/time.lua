local function f2(...)
  return select("#", ...)
end

local function f1(...)
  local l = select("#", ...)
  local m = 0
  for i = 1, l do
    m = m + select(i, ...)
  end

  local n = f2(...)

  return m + n
end

local n = 0
for i = 1, 1000 * 1000 * 100 do
  n = n + f1(1, 2, 3, 4, 5)
end

print("n: ", n)
