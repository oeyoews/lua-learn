--- two array come true
---@param row number
---@param col number
local init_array = function(row, col)
  local matrix = {}
  for i = 1, row do
    matrix[i] = {}
    for j = 1, col do
      matrix[i][j] = 0
    end
  end

  for _, v in ipairs(matrix) do
    for _, k in ipairs(v) do
      -- tbl = { k }
      print(k)
    end
  end
end

-- 9x9 array
init_array(9, 9)
