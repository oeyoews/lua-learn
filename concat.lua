local tbl = {
  demo = 1,
  __concat = function(self, t)
    for key, value in pairs(self) do
      t[key] = self[value]
      t[value] = self[value]
    end
  end,
}

local t = {
  vanilla = 0,
}

local res = setmetatable(t, {
  tbl,
})

--[[ for key, value in pairs(res) do
  print(key, value)
end --]]
