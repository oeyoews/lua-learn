first = {
  "first",
  1,
  2,
  3,
}

second = {
  "second",
  1,
  3,
  4,
}

third = {
  "third",
  4,
  5,
  6,
}

local tbl = {
  -- different
  test = { "demok", "dmeok", 3 },
  second,
  -- first,
  -- third,
}

-- table.insert(tbl, first)
-- table.insert(tbl, second)
-- table.insert(tbl, third)

print("TE")
print(tbl[1])

for index, value in pairs(tbl) do
  print(index, value)
  -- for i, v in ipairs(value) do
  --   print(v)
  -- end
end
