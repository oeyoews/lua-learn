local journey_dir = os.date("%Y/%m/%d")
local journey = journey_dir .. "/index.md"
vim.fn.mkdir(journey_dir, "p")
vim.cmd(([[edit %s]]):format(journey))
