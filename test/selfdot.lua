local tbl = {
  a = 1,
  b = 99,
}

function tbl.add(hello, a)
  return hello.a, a
end

print(tbl.add(tbl, 99))
