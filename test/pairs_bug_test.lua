local M = {}

M.options()({
  1,
  2,
  "demo",
})

M.options_bk()({
  number = true,
  signcolumn = "yes",
})

-- pairs
function M.setup()
  for k, v in pairs(M.options) do
    print("pairs test <==")
    print(k, v)
  end
  print(" time is   .. test ...       test")
end

-- ipairs
function M.two()
  -- end ???
  -- second  iterator stopping
  for i, j in ipairs(M.options) do
    print("📟 ipairs test <==")
    print("    .. ipairs ...       test")
    print("demo" .. i, j)
    print("    .. ipairs ...       test")
  end
end

return M
