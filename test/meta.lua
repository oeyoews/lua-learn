local M = {}

print("1: ", getmetatable(M))

local t1 = {}
setmetatable(t1, M)

print("2: ", getmetatable(t1))

local t = {}
print("3:", getmetatable(t))
