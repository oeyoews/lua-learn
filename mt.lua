local mt = {
  met = "This is a demo",
  t = function(self, key)
    if key == "met" then
      return "suc"
    else
      return nil
    end
  end,
}

mytbl = {
  demo = "native",
  m = "d",
}

local tt = setmetatable(mytbl, {
  __concat = mt,
})

for key, value in pairs(tt) do
  print(key, value)
end

local ttt = getmetatable(tt)

for key, value in pairs(ttt) do
  print(key, value)
end
