local modules = {}

modules = {
  apple = {
    "red",
    "color",
  },
  banana = {
    "yellow",
    "color",
  },
}

-- modules = {
--   1, 2
-- }
--
print(modules.apple[1])

for i = 1, #modules do
  print(modules.apple[i])
end
