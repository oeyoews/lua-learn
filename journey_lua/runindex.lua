local M = {
	lua = "lua ",
}

--- test run
function M.run()
	local vim = vim
	local cmd = nil
	local file_type = vim.fn.expand("%:e")
	local file_name = vim.fn.expand("%:p")
	if file_type == "lua" then
		cmd = M[file_type]
	end
	local output_list = vim.fn.split(vim.fn.system(cmd .. file_name), "\n")
	for _, v in ipairs(output_list) do
		print(v)
	end
end
