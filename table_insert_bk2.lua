setup = function(entry, m2, m3)
  for second, module in pairs(m3) do
    print(second)
    -- for _, load_module in ipairs(m3[second]) do
    --   connect_path = string.format("%s.%s.%s", entry, second, load_module)
    --   print(connect_path)
    -- end
  end
end

m2 = {
  "ui",
  "langs",
  "tools",
}

m3 = {
  -- @utils
  ["utils"] = {
    "disable",
    "functions", -- need after mappings
  },

  -- @ui
  ui = {
    "windline", -- statusline
    "fidget", -- visualize lsp status
  },

  -- @LANG
  langs = {
    "treesitter", -- friendly highlight your code
    "formatter",
  },

  -- @TOOLS
  tools = {
    "gitsigns", -- show git changes in signcolumn
    "comment", -- fast comment your code
  },
}

local entry = "test"
setup(entry, m2, m3)
