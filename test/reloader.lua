local tbl_1 = { first = "first" }

local tbl_2 = {
  second = "second",
}

setmetatable(tbl_1, { __index = tbl_2 })

print(getmetatable(tbl_1))

print("tbl_2's meta ", getmetatable(tbl_2))

print(tbl_1["first"])
print("metatable's key second", tbl_1["second"])

local k1 = {}

local t = {}

t[k1] = "hello"

print(t[k1])
print(getmetatable(k1))
print("string's metatable is", getmetatable("H"))
print("number's metatable is", getmetatable(99))
