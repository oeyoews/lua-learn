--[[
-- local hello = require 'hello'
local util = require('test.util')

local hello = require('hello')

local lo = require('modules.lo')

local losetup = require('modules.lo').setup(9, 99, 99, "Hlm", "9888")

print("setup function", losetup)

local lov = lo.lov

-- local tabpara = require('modules.lo').TabTest()
print("This module.lov variable is", lov)

-- local localtest = require 'localtest'

-- callback setup
util.setup()

local array = {
  "this is a sentence",
  23,
  99,
}

for _, indexres in ipairs(array) do
  print(indexres)
end

for i = 0, 4 do
  print(array[i])
end

local he = hello.servers
print(he[4])
]]

--- print(package.path)
-- print("start")
--

local servers = require("servers").servers

-- demo
-- append lsta position
table.insert(servers, "addpen_value_test")
table.remove(servers, 3)

for _, server in ipairs(servers) do
  print(server)
end

-- local servers = require("modules.servers")
--
-- print(type(servers.servers))
--
--
-- for _, server in ipairs(servers) do
--   print(server)
-- end
