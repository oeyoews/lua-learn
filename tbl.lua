local n = {
  demss = "demo",
  4,
  e = function(tbl, key)
    return "suc"
  end,
}
local tbl = { dmeo = "met", 1, 2 }

local t = setmetatable(tbl, {
  __index = n,
})

local res = t.e(tbl, 4)

print(t[3])

for index, value in pairs(t) do
  print(index, value)
end
