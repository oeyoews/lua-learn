--- @see https://www.jianshu.com/p/18e63cba49cc
local closure_test = function(i)
  return function(j)
    i = i + j
    return i
  end
end

local rest = closure_test(999)
print(rest(9999))
print(rest(9999))
print(rest(888))
print(rest(1))
print(rest())
print(rest())

local tbl = function()
  return "Demo"
end

print(tbl())

--- default return value is nil, if no driving return statement
local tbl2 = function()
  print("dmeo")
  function dmeo()
    return "This is a dmeo"
  end
end

tbl2()
