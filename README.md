<div align="center">
<h1 align="center">Lua<h1>
<img src="img/lua.png" width=128 align="center"/> <hr>

<img src="https://img.shields.io/badge/Lang-lua-blueviolet.svg?style=flat-square&logo=lua&color=90E59A&logoColor=blue" alt="lang">

</div>

## TODO

- [ ] use \_\_concat ?
- [ ] what's the mean string?
- [ ] use treemap to emulate in lua(string.byte string.len)
- lua virtual realize
- read programming in lua pdf
- [ ] come true data struct use lua

* how to sure must be have a receive return value
* get address operator in lua?

- sleep order execute http://lua-users.org/wiki/SleepFunction
- [x] reloader user metatable

* [ ] use lua to compile script
* [ ] how to operator table to set option

## TOOLS

- rider(unity): https://www.jetbrains.com/zh-cn/riderflow/
- clion or vscode
- `emmylua`
- `debugger` lua debug(vscode)
- luadoc

`luadoc -d docs *`

## Des

- nil false
- number string
- function table
- userdata thread

## manual

- manual: https://cloudwu.github.io/lua53doc/manual.html
- manual: https://www.runoob.com/manual/lua53doc/contents.html

## Links

- https://zhuanlan.zhihu.com/c_1482991532769177600 or bilibili
- https://learnxinyminutes.com/docs/lua/

* blog: https://blog.codingnow.com/eo/luaoeeeaeau/
* https://forum.robloxdev.cn/t/topic/767
* https://love2d.org/wiki/Main_Page_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)
* https://simplegametutorials.github.io/love/blocks/

- https://www.jc2182.com/lua/lua-table.html
- https://www.cnblogs.com/rollingyouandme/p/11728235.html
- http://lua-users.org/wiki/LuaFaq
- https://www.jianshu.com/p/9cf4681846fd
- https://www.yiibai.com/lua/lua_web_programming.html
- https://www.jianshu.com/p/18e63cba49cc
- https://zh.wikipedia.org/wiki/%E5%93%88%E5%B8%8C%E8%A1%A8
- https://github.com/oeyoews/Books-2
- https://developer.51cto.com/article/643017.html
- https://www.zhihu.com/column/c_1364048322639155200
- xlua: https://github.com/Tencent/xLua/blob/master/Assets/XLua/Doc/XLua%E6%95%99%E7%A8%8B.md
- https://asmcn.icopy.site/awesome/awesome-lua/
